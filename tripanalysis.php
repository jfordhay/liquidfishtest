<?php

// Open the fishes json file
$fishesJson = json_decode(file_get_contents("https://liquid.fish/fishes.json"));

$tripArray = [];

foreach($fishesJson as $tripData) {

    if(!in_array($tripData->date, $tripArray)) {
        $tripDate = explode("T", $tripData->date)[0];
        if(!in_array($tripDate, $tripArray)) {
            $tripArray[$tripDate] = [];
        }
        $fishData = [];
        foreach($tripData->fish_caught as $fishString) {
            if(isset($fishData[$fishString])) {
                $fishData[$fishString] += 1;
            } else {
                $fishData[$fishString] = 1;
            }
        }
        foreach($fishData as $fishName => $catchCount) {
            if(isset($tripArray[$tripDate][$fishName])) {
                $tripArray[$tripDate][$fishName] += $catchCount;
            } else {
                $tripArray[$tripDate][$fishName] = $catchCount;
            }
        }
    }
    
}

foreach($tripArray as $tripDate => $fishData) {

    asort($fishData);
    $top20Fish = array_slice($fishData, 0, 20);
    $top20String = "";
    $totalFishCaught = 0;
    $elementCount = 0;
    foreach($top20Fish as $fishName => $fishCount) {
        $elementCount += 1;
        if($top20String == "") {
            $top20String = $fishName;
        } else {
            if($elementCount == count($top20Fish)) {
                $top20String = $top20String . " and {$fishName}";
            } else {
                $top20String = $top20String . ", {$fishName}";
            }
        }
        $totalFishCaught += $fishCount;
    }

    echo "The most common fish on {$tripDate} were {$top20String} (Caught {$totalFishCaught} times)\n";

}

?>
